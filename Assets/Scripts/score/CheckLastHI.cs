﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckLastHI : MonoBehaviour
{
    public RectTransform HI;
	public Vector3 distance;
	public RectTransform handle;
    void Update()
    {
        distance = handle.anchoredPosition3D;
        distance.x+=20/2;
        HI.anchoredPosition3D = distance;
    }
}
