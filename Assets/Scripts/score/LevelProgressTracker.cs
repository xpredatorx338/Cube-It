﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelProgressTracker : MonoBehaviour{
    public Slider fill;
    public Transform endGameD;
    public Transform player;

    void Start()
    {
        fill = GetComponent<Slider>();
    }
    
    void Update()
    {
        fill.value = player.position.z / endGameD.position.z;
    }
}
