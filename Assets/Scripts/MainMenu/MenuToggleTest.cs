﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class MenuToggleTest : MonoBehaviour
{
    public Toggle myToggle;
    public BaseEventData data;
    // Start is called before the first frame update
    void Awake()
    {
        myToggle = GetComponent<Toggle>();

    }

    void Start()
    {
        myToggle.Select();
    }
}
