﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloatingMenuPanels : MonoBehaviour
{
    private RectTransform _thisTransform;
    private Animator _thisAnimator;
    private CanvasGroup _thisCanvas;

    public float mouseX;

    public float mouseY;
    public float moveSpeed;
    public float smoothLerp;

    public Vector3 targetPosition;

    public float initialX;
    // Start is called before the first frame update
    void Awake()
    {
        _thisTransform = GetComponent<RectTransform>();
        _thisCanvas = GetComponent<CanvasGroup>();

        initialX = _thisTransform.anchoredPosition.x;
    }

    // Update is called once per frame
    void Update()
    {

        mouseX += Input.GetAxis("Mouse X") * moveSpeed;
        mouseY += Input.GetAxis("Mouse Y") * moveSpeed;

        mouseX = Mathf.Clamp(mouseX, -5, 5);
        mouseY = Mathf.Clamp(mouseY, -5, 5);

        targetPosition = new Vector3(mouseX, mouseY, 0);
        _thisTransform.anchoredPosition = Vector3.Lerp(_thisTransform.anchoredPosition, targetPosition + new Vector3(initialX, 0, 0), Time.deltaTime * smoothLerp);
    }
}
