﻿using UnityEngine;
using UnityEngine.EventSystems;
using TMPro;
using UnityEngine.Events;
using UnityEngine.UI;

public class BetterButton : Selectable, IPointerClickHandler
{
    //event Variables that show up in the inspector
    public UnityEvent onClickEvent;
    public UnityEvent onHoverEvent;

    //get the scriptable object for state colors
    public ButtonsSettings customColorSettings;

    //variables that set up the hovering effect with the Z position of the components
    private float _currentZ;
    private float _initialZ;
    private float _targetZ;
    private float _smoothSizeTime = 10f;

    private TextMeshProUGUI _thisText;
    private RectTransform _thisTransform;
    private ColorBlock _myColor;

    //--------------//
    // start method //
    //--------------//
    protected override void Start()
    {
        base.Start();

        //get the important components
        _thisText = transform.GetChild(0).GetComponent<TextMeshProUGUI>();
        _thisTransform = GetComponent<RectTransform>();
        _initialZ = _thisTransform.localPosition.z;
        _targetZ = _initialZ - 50;

        targetGraphic = _thisText;

        //fuck around with colors
        _myColor.normalColor = customColorSettings.normalColor;
        _myColor.highlightedColor = customColorSettings.highlightColor;
        _myColor.pressedColor = customColorSettings.pressedColor;
        _myColor.selectedColor = customColorSettings.selectedColor;
        _myColor.colorMultiplier = customColorSettings.colorMultiplier;
        _myColor.fadeDuration = customColorSettings.transitionTime;
        
        this.colors = _myColor;
    }

    //---------------//
    // update method //
    //---------------//
    void Update()
    {

        //take the current Z value so we can use it for animation
        _currentZ = _thisTransform.localPosition.z;
        
        //if the button is currently mouse hovered, do things
        if (IsHighlighted())
        {
            _thisTransform.localPosition = new Vector3(_thisTransform.localPosition.x, _thisTransform.localPosition.y, Mathf.Lerp(_currentZ, _targetZ, Time.deltaTime * _smoothSizeTime));
            onHoverEvent.Invoke();
            _thisText.fontStyle = customColorSettings.highlightFontStyle;
        }
        else
        {
            _thisTransform.localPosition = new Vector3(_thisTransform.localPosition.x, _thisTransform.localPosition.y, Mathf.Lerp(_currentZ, _initialZ, Time.deltaTime * _smoothSizeTime));
            _thisText.fontStyle = FontStyles.Normal;
        }
    }
    
    //if the button is clicked invoke the Unity Event
    public void OnPointerClick(PointerEventData eventData)
    {
        onClickEvent.Invoke();
    }
    
}
