﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SocialMediaLinks : MonoBehaviour
{

	public void OpenInstagram()
	{
		Application.OpenURL("https://www.instagram.com/_xycy_/?hl=en");
	}

	public void OpenTwitter()
	{
		Application.OpenURL("https://twitter.com/_xycy_");
	}

	public void OpenYoutube()
	{
		Application.OpenURL("https://www.youtube.com/channel/UCqT2jQPVNJSsXs3sY0gF4mg/featured?view_as=public");
	}

	public void OpenFeedback()
	{
		Application.OpenURL("https://epic-prestige.itch.io/cubeit");
	}
}
