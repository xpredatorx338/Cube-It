﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
	[Header("Other Variables")]
	public GameManager gameManager;
    public Rigidbody playerRigidbody;
    public bool isAlive = true;
    
    [Header("Movement Settings")]
	public float forwardForce = 1f;
	public float sidewaysForce = 2f;
	public float addForwardForce = 1f;
	
	[Space] 
	public int numberOfPowers = 10;
	public float jumpForce;

	[Header("Camera Settings")]
	public Camera playerCam;
	public Vector3 offset;

	private bool isJumping;
	// start is called before the first frame update 
	private void Start()
	{
		playerRigidbody = GetComponent<Rigidbody>();
	}

	//---------------------------------//
	//function that will run each frame//
	//---------------------------------//
	void Update()
	{
		//decrease the number of powers and allow the jump in FixedUpdate
		if (Input.GetKeyDown(KeyCode.Space) && numberOfPowers > 0 && playerRigidbody.position.y <= 1.5f)
		{
			numberOfPowers -= 1;
			isJumping = true;
		}
	}

	//---------------------------------------------//
	//function that will handle the player movement//
	//---------------------------------------------// 
    void FixedUpdate()
	{
		if (isAlive)
		{
			MovePlayer();
			MoveCamera();
		}

		if (playerRigidbody.position.y < -0.5f)
		{
			gameManager.EndGame();
		}
	}

    //--------------------------------------------------//
    //function that will move the camera with the player//
    //--------------------------------------------------// 
    void MoveCamera()
    {
	    playerCam.transform.position = transform.position + offset;
    }

    //----------------------------------//
    //function that will move the player//
    //----------------------------------//
    void MovePlayer()
    {
	    //add continuous forward move to the cube
	    playerRigidbody.AddForce(0, 0, forwardForce, ForceMode.VelocityChange);
		
	    //add sideways movement based on user input
	    if (Input.GetAxisRaw("Horizontal") > 0)
	    {
		    playerRigidbody.AddForce(sidewaysForce, 0, 0, ForceMode.VelocityChange);
	    } 
	    else if (Input.GetAxisRaw("Horizontal") < 0)
	    {
		    playerRigidbody.AddForce(-sidewaysForce, 0, 0,ForceMode.VelocityChange);
	    }
		//add forward movement based on user input
		if (Input.GetAxisRaw("Vertical") > 0)
		{
			playerRigidbody.AddForce(0, 0, addForwardForce, ForceMode.Impulse);
		}
		else if (Input.GetAxisRaw("Vertical") < 0)
		{
			playerRigidbody.AddForce(0, 0, -addForwardForce, ForceMode.Impulse);
		}

		//jump if isJumping in update is true
		if (isJumping)
	    {
		    playerRigidbody.velocity = new Vector3(playerRigidbody.velocity.x, jumpForce, playerRigidbody.velocity.z);

			isJumping = false;
		}
    }

    //------------------------------------------------------//
    //function that will decide if the player is dead or not//
    //------------------------------------------------------//
    void OnCollisionEnter(Collision collisionInfo)
    {
	    if (collisionInfo.collider.CompareTag("Obsticle"))
	    {
		    gameManager.EndGame();
			FindObjectOfType<AudioManager>().PlaySound("Player death");
		}
    }
}