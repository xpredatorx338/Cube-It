﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Button = UnityEngine.UIElements.Button;

[RequireComponent(typeof(AudioSource))]
public class ButtonFX : MonoBehaviour, IPointerEnterHandler, IPointerClickHandler
{
	public AudioSource myFX;
	public AudioClip hoverFX;
	public AudioClip clickFX;

	public void Start()
	{
		myFX = GetComponent<AudioSource>();
	}

	public void OnPointerEnter(PointerEventData eventData)
	{
		myFX.PlayOneShot(hoverFX);
	}

	public void OnPointerClick(PointerEventData eventData)
	{
		myFX.PlayOneShot(clickFX);
	}
	
}
