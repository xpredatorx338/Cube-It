﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Serialization;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    private bool _gameHasEnded = false;
    private bool _gameIsPaused = false;

    [Header("Game Settings")]
    public float timeToActivateDyingUi = 1f;

    [Header("Necessary objects to grab")]
    public ResetingGame scores;
    public PlayerMovement mainPlayer;

    [Header("UI Panels to manage")]
    public GameObject completeLevelUi;
    public GameObject dyingUi;
    public GameObject pauseMenuUi;
    public GameObject inGamePanel;

    //--------//
    // update //
    //--------//
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape) || Input.GetKey("p"))
        {
            if (_gameIsPaused)
            {
                ResumeGame();
            }
            else
            {
                PauseGame();
            }
        }
    }

    //show the UI if the level is completed
    public void CompleteLevel()
    {
        completeLevelUi.SetActive(true);
    }

    //show the UI if the player died
    public void EndGame()
    {
        mainPlayer.isAlive = false;
        inGamePanel.SetActive(false);
        Invoke(nameof(ActivateDyingUI), timeToActivateDyingUi);
    }

    //restart the level function
    public void Restart()
    {
        if (_gameHasEnded == false)
        {
            _gameHasEnded = true;
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);

        }
    }

    //load main menu function
    public void LoadMenu()
    {
        Time.timeScale = 1f;
        SceneManager.LoadScene("Menu");
    }

    private void ActivateDyingUI()
    {
        dyingUi.SetActive(true);
        scores.HiScore();
        scores.Score();
    }
    //----------------------//
    // pause menu functions //
    //----------------------//
    void PauseGame()
    {
        pauseMenuUi.SetActive(true);
        Time.timeScale = 0f;
        _gameIsPaused = true;
    }

    public void ResumeGame()
    {
        pauseMenuUi.SetActive(false);
        Time.timeScale = 1f;
        _gameIsPaused = false;
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}
