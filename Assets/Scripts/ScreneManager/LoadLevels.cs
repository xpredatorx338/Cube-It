﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadLevels : MonoBehaviour
{
	public GameObject loadingScreen;
	
	public void LoadScene(string sceneName)
	{
		StartCoroutine(LoadAsynchrounously(sceneName));
	}
	
	IEnumerator LoadAsynchrounously(string sceneName)
	{
		AsyncOperation operation = SceneManager.LoadSceneAsync(sceneName);

		while (!operation.isDone)
		{
			loadingScreen.SetActive(true);
			yield return null;
		}

	}
	
}
