﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ResetingGame : MonoBehaviour
{
	public Transform playerZ;

	public TextMeshProUGUI hiScore;
	public TextMeshProUGUI currentScore;
	
	void Start()
	{
		hiScore.text = PlayerPrefs.GetInt("HighScore", 0).ToString();
	}

	public void HiScore()
	{
		float second = playerZ.position.z;
		if (second > PlayerPrefs.GetInt("HighScore", 0))
		{
			PlayerPrefs.SetInt("HighScore", (int)second);
			hiScore.text = second.ToString("0");
		}
	}

	public void Score()
	{
		currentScore.text = playerZ.position.z.ToString("0");
	}
}
